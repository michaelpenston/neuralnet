import numpy as np
from PIL import Image
from sklearn import model_selection as ms
import tensorflow as tf
from matplotlib import pyplot as plt
from mpl_toolkits import mplot3d
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

dataset = tf.keras.datasets.mnist
(x_train,y_train),(x_test,y_test) = dataset.load_data()
print(x_test.shape)
x_train = x_train.reshape((60000,784))
y_train = tf.keras.utils.to_categorical(y_train)

x_train = np.where(x_train>0,1,0)
x_test = np.where(x_test>0,1,0)

x_test = x_test.reshape((10000,784))
y_test = tf.keras.utils.to_categorical(y_test)

class Network:
    def __init__(self,shape):
        self.struct = shape
        self.x_train = x_train[:20000]
        self.y_train = y_train[:20000]
        self.x_test = x_test[:1000]
        self.y_test = y_test[:1000]
##        #importing the train,test data
##        try:
##            self.x_train , self.x_test, self.y_train, self.y_test = ms.train_test_split(data,labels)
##        except:
##            raise "Error: data not loaded"
##        #initialising weights and biases

        weights = []
        bias = []
        
        for layer in range(len(self.struct)):
            try:
                layerWeights = (np.random.rand(self.struct[layer],self.struct[layer+1])*2)-1
                weights.append(layerWeights)
                bias.append((np.random.rand(1,self.struct[layer+1])*2)-1)
            except:
                pass
        self.weights = weights
        self.bias = bias
        #epochs
        points = []
        for epo in range(5):
            for batchnum in range(0,self.x_train.shape[0],32):
                self.traindata = self.x_train[batchnum:batchnum+32]
                self.labeldata = self.y_train[batchnum:batchnum+32]
                self.forwardProp()
                for x in range(3):
                    self.backProp()
                    points.append(self.forwardProp())
                
            print(self.forwardProp())
            self.evaluate()
        plt.plot(points)
        plt.show()
            
    #activation function: we will use Sigmoid
    def activation(self,x):
        #return np.maximum(x,0,x)
        return (1/(np.exp(-x)+1))
    def activation_derrivative(self,x):
##        x[x<=0] = 0
##        x[x>0] = 1
##        return x
        return self.activation(x)*(1-self.activation(x))

    #loss function
    def loss(self,y,predict_y):
        sumSquaredErr = np.sum((predict_y-y)**2)
        return sumSquaredErr / y.size

    def loss_derrivative(self,predict_y,y):
        return (predict_y - y)
    
    def forwardProp(self):
        #set x as the input data
        x = self.traindata

        #a place to put the activation values
        self.activations = []
        self.activations.append(x)
        #go through each layer -1 as we do not perform arithmatic on output neuron
        for i in range(len(self.struct)-1):
            w = self.weights[i]
            b = self.bias[i]

            #FOWARD PROPOGATION
            z = np.matmul(x,w) + np.repeat(b,x.shape[0],0)
            x = self.activation(z)
            self.activations.append(x)
        self.z = z #needed to calculated output error in backprop
        self.predict_y = x
        return self.loss(self.labeldata, self.predict_y)

    def backProp(self):
        learning_rate = 0.01
        for x in range(self.traindata.shape[0]):
            self.forwardProp()
            deltaL = self.loss_derrivative(self.predict_y[x],self.labeldata[x]) * self.activation_derrivative(self.z[x])

            self.bias[-1] -= learning_rate * deltaL
            
            x2t = np.reshape(self.activations[-2][x],(self.activations[-2][x].shape[0],1))
            self.weights[-1] -= learning_rate * np.dot(x2t,deltaL.reshape(1,10))

            #Now go back through each layer, starting from last. Ignore input layer
            
            for i in range(1,len(self.struct)-1):
                l = i+1
                deltaL = np.dot(self.weights[-l+1],deltaL.transpose())
                self.bias[-l] -= learning_rate * deltaL
                self.weights[-l] -= learning_rate * np.dot(np.reshape(self.activations[-l-1][x], (self.activations[-l-1][x].shape[0],1)),deltaL.reshape(1,deltaL.shape[0]))
    def evaluate(self):
        self.traindata=self.x_test
        self.labeldata=self.y_test
        self.forwardProp()
        y = self.y_test
        yhat = self.predict_y
        correct = 0
        for x in range(y.shape[0]):
            if np.argmax(yhat[x])==np.argmax(y[x]):
                correct += 1
            else:
                pass
        print((correct/yhat.shape[0])*100)

    def predict(self,a):
        #not finished yet :P
        self.x_train = a
        print(self.forwardProp())
        print(self.predict_y)
    def saveErrorSurface(self):
        x=[]
        y=[]
        z=[]
        
        for x_coord in range(-250,250,10):
            for y_coord in range(-250,250,10):
##                self.bias[0][0][5] = x_coord/100
##                self.bias[0][0][6] = y_coord/100
                self.weights[1][5][5] = x_coord/100
                self.weights[1][6][5] = y_coord/100
                x.append(x_coord/100)
                y.append(y_coord/100)
                z.append(self.forwardProp())
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        surf = ax.plot_trisurf(x, y, z, cmap=cm.coolwarm)
        ax.set_zlim(0, 0.1)
        ax.zaxis.set_major_locator(LinearLocator(10))
        ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

        plt.show()

##        with open('errorsurface.csv','w') as f:
##            csvFile = csv.writer(f)
##            for row in data:
##                csvFile.writerow(row)
        
for x in range(1):
    net = Network([784,32,16,10])
